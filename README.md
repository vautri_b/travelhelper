# TravelHelper

This project is a mobile application allowing travelers to plan their journey. It provides useful information about the visited country(ies) such as the political situation, the upcoming events, specific items they might require, ...

## User guide

### Distribution

The application is available on the Google Play Store.

You can then launch it like any app on your Android device.

<a href='https://play.google.com/store/apps/details?id=com.travelhelper&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>

https://play.google.com/store/apps/details?id=com.travelhelper

On the app, the user will find a list of the coutries for which we provide information. Then they can select one, and browse the information, and check the different items they might need for their trip.

A menu entry allows the user to be redirected to their email inbox in order to send an email to a specific address, which will add an issue to Gitlab's issue tracker.

## Developer guide

Gitlab project: https://gitlab.com/vautri_b/travelhelper

### Repository structure

The code is in the ``./src/`` directory.

The ``./node_modules/`` directory is used to store the modules required to build and run the app.

The ``./resources/`` directory holds the graphical assets of the project.

### Build and run

First, you should run the ``npm i`` command to install the required packages to build and run the app.

To build the project, i.e. build an .apk file, run ``ionic cordova build android``. Add ``-- release`` to build a release APK which is more suitable to distribute.

Use ``npm test`` to run the tests.

### Automated testing

We will use Gitlab CI to run automated tests and publish the app on the Play Store. It consists of writing a ``.gitlab-ci.yml`` file which describes how to do this.

### Issue tracking

The reported bugs are represented by issues in Gitlab. The resolved ones are the closed issues, otherwise they will be open. For each issue resolution, you should create a new branch and tag the issue as 'Doing'.Once it is resolved, you should submit a merge request to the ``master`` branch.

As the merge request is accepted, you shall close the related issue.