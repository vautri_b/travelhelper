import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {UserDataModel} from '../../models/user-data/user-data-model';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  public static USER_DATA_KEY = 'user-data';

  private userData: UserDataModel = null;

  constructor(private storage: Storage) {
  }

  private async getOnStorage(): Promise<UserDataModel> {
    const value = await this.storage.get(UserDataService.USER_DATA_KEY);
    if (value) {
      return new UserDataModel(JSON.parse(value));
    }
    return null;
  }

  private async setOnStorage(): Promise<void> {
    const str = JSON.stringify(this.userData);
    await this.storage.remove(UserDataService.USER_DATA_KEY);
    await this.storage.set(UserDataService.USER_DATA_KEY, JSON.stringify(str));
  }

  async getUserData(): Promise<UserDataModel> {
    await this.storage.remove(UserDataService.USER_DATA_KEY);
    if (!this.userData) {
      this.userData = await this.getOnStorage() || new UserDataModel();
    }
    return this.userData;
  }

  setUserData(userData: UserDataModel): Promise<void> {
    this.userData = userData;
    return this.setOnStorage();
  }
}
