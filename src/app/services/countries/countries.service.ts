import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {Country} from '../../models/country/country';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  private countries: Observable<Country[]>;
  private countryCollection: AngularFirestoreCollection<Country>;
  private test: string = null;

  constructor(public angularFireStore: AngularFirestore) {
    this.countryCollection = this.angularFireStore.collection<Country>('countries');
    this.countries = this.countryCollection.snapshotChanges().pipe(
      map(countries => {
        return countries.map(action => {
          const data = action.payload.doc.data();
          data.id = action.payload.doc.id;
          console.log(data);
          return Country.ParseFromObject(data);
        });
      })
    );
  }

  getCountries(): Observable<Country[]> {
    return this.countries;
  }

  getCountry(id: string): Observable<Country> {
    return this.countryCollection.doc<any>(id).valueChanges().pipe(
      map(country => {
        country.id = id;
        return Country.ParseFromObject(country);
      })
    );
  }

  getCountry2(id: string): Observable<Country> {
    return this.countries.pipe(
      map(countries => countries.find(country => country.id === id))
    );
  }

  getTest() {
    return this.test;
  }
}
