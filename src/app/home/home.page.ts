import {Component} from '@angular/core';
import {Country} from '../models/country/country';
import {CountriesService} from '../services/countries/countries.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  france =  {
    name: 'France',
    description: 'Description de la France',
    itemsToBring: ["Longboard"],
    picture: "https://www.vikingrivercruises.com/images/Paris_Effel_Tower_Seine_Panorama_478x345_tcm21-137845.jpg",
    politicalSituation: {
      situation: 0,
      comment: "La France c'est safe et bien"
    },
    recentHistory: ["Il s'est passe un truc wola"],
    todoList: [{
      name: 'Louvre',
      description: "Ceci est une description",
      type: 'museum',
      place: 'Address'
    }]
  };

  data = [this.france];

  countries: Country[];

  constructor(public countriesService: CountriesService) {
    this.countriesService.getCountries()
      .subscribe(countries => {
        this.countries = countries;
        console.log(countries);
      });
    this.countriesService.getCountry2("vh1RMT1PHymziHiIqVIR")
      .subscribe(country => {
        console.log(country);
      });
  }
}
