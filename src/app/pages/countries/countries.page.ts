import {Component, OnInit} from '@angular/core';
import {Country} from 'src/app/models/country/country';
import {Observable} from 'rxjs';
import {CountriesService} from '../../services/countries/countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.page.html',
  styleUrls: ['./countries.page.scss'],
})
export class CountriesPage implements OnInit {

  countries: Observable<Country[]>;

  constructor(public countriesService: CountriesService) { }

  ngOnInit() {
    this.countries = this.countriesService.getCountries();
  }
}
