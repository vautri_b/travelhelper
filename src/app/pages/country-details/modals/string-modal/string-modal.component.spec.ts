import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StringModalComponent } from './string-modal.component';

describe('StringModalComponent', () => {
  let component: StringModalComponent;
  let fixture: ComponentFixture<StringModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StringModalComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StringModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
