import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-string-modal',
  templateUrl: './string-modal.component.html',
  styleUrls: ['./string-modal.component.scss'],
})
export class StringModalComponent implements OnInit {

  value: string;
  name: string;

  constructor(private navParams: NavParams, private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

}
