import {Component, OnInit} from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';
import {UserDataService} from '../../../../services/user-data/user-data.service';
import {UserDataModel} from '../../../../models/user-data/user-data-model';

interface Item {
  val: string;
  checked: boolean;
}

@Component({
  selector: 'app-item-list-modal',
  templateUrl: './item-list-modal.component.html',
  styleUrls: ['./item-list-modal.component.scss'],
})
export class ItemListModalComponent implements OnInit {

  constructor(private navParams: NavParams, private modalCtrl: ModalController, private userDataService: UserDataService) { }

  public items: Item[] = [];
  private userData: UserDataModel;

  values: string[];
  countryId;

  ngOnInit() {
    this.userDataService.getUserData()
      .then(userData => {
        this.userData = userData;
        if (!this.userData.countryItems[this.countryId]) {
          this.userData.countryItems[this.countryId] = [];
        }
        this.items = this.values.map(value => {
          const item: Item = {val: value, checked: false};
          if (this.userData.countryItems[this.countryId].findIndex(val => val === value) > -1) {
            item.checked = true;
          }
          return item;
        });
      });
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  saveAndQuit() {
    const array = this.items.filter(item => item.checked).map(item => item.val);
    this.userData.countryItems[this.countryId] = array;
    this.userDataService.setUserData(this.userData)
      .then(() => this.modalCtrl.dismiss());
  }
}
