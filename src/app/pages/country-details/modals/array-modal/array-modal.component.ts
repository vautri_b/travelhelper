import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-array-modal',
  templateUrl: './array-modal.component.html',
  styleUrls: ['./array-modal.component.scss'],
})
export class ArrayModalComponent implements OnInit {

  name: string;
  value: string[];

  constructor(private navParams: NavParams, private modalCtrl: ModalController) { }

  ngOnInit() {

  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

}
