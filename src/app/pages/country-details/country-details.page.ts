import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Country} from '../../models/country/country';
import {CountriesService} from '../../services/countries/countries.service';
import {ArrayModalComponent} from './modals/array-modal/array-modal.component';
import {TodoModalComponent} from './modals/todo-modal/todo-modal.component';
import {StringModalComponent} from './modals/string-modal/string-modal.component';
import {ItemListModalComponent} from './modals/item-list-modal/item-list-modal.component';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.page.html',
  styleUrls: ['./country-details.page.scss'],
})
export class CountryDetailsPage implements OnInit {

  private countryId: string = null;
  private countryObservable: Observable<Country>;
  public country: Country;

  situation = ['this country is stable.', 'this country is unstable', 'this country is unsafe'];

  constructor(public modalController: ModalController,
              public activatedRoute: ActivatedRoute,
              public countriesService: CountriesService) {  }

  ngOnInit() {
    this.countryId = this.activatedRoute.snapshot.paramMap.get('id');
    this.countryObservable = this.countriesService.getCountry(this.countryId);
    console.log(this.countryId, this.countryObservable, this.countriesService.getTest());
    this.countryObservable.subscribe(country => {
      this.country = country;
      console.log(country);
    });
  }

  async showString(name: string, value: string) {
    const modal = await this.modalController.create({
      component: StringModalComponent,
      componentProps: { name, value }
    });
    return await modal.present();
  }

  async showArrays(name: string, value: string[]) {
    const modal = await this.modalController.create({
      component: ArrayModalComponent,
      componentProps: { name, value }
    });
    return await modal.present();
  }

  async showToDo() {
    const modal = await this.modalController.create({
      component: TodoModalComponent,
      componentProps: { value: this.country.todoList }
    });
    return await modal.present();
  }

  async showItems() {

    const modal = await this.modalController.create({
      component: ItemListModalComponent,
      componentProps: { values: this.country.itemsToBring, countryId: this.countryId },
    });
    return await modal.present();
  }

}
