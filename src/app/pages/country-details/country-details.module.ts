import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CountryDetailsPage } from './country-details.page';
import {ArrayModalComponent} from './modals/array-modal/array-modal.component';
import {StringModalComponent} from './modals/string-modal/string-modal.component';
import {TodoModalComponent} from './modals/todo-modal/todo-modal.component';
import {ItemListModalComponent} from './modals/item-list-modal/item-list-modal.component';

const routes: Routes = [
  {
    path: '',
    component: CountryDetailsPage
  }
];

@NgModule({
  entryComponents: [
    ArrayModalComponent,
    StringModalComponent,
    TodoModalComponent,
    ItemListModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CountryDetailsPage,
    ArrayModalComponent,
    StringModalComponent,
    TodoModalComponent,
    ItemListModalComponent
  ]
})
export class CountryDetailsPageModule {}
