export class Todo {
  public name: string;
  public type: string;
  public description: string;
  public place: string;

  constructor(name: string,
              type: string,
              place: string,
              description?: string) {
    this.name = name;
    this.type = type;
    this.place = place;
    this.description = description;
  }

  static ParseFromObject(obj: any): Todo {
    return new Todo(obj.name,
      obj.type,
      obj.place,
      obj.description);
  }
}
