import { PoliticalSituation } from '../potlical-situation/political-situation';
import { Todo } from '../todo/todo';

export class Country {

  public id: string;
  public name: string;
  public flag: string;
  public politicalSituation: PoliticalSituation;
  public picture: string;
  public description?: string;
  public todoList?: Todo[];
  public recentHistory?: string[];
  public localLaws?: string[];
  public itemsToBring?: string[];
  public continent: string;
  public emergencyNumbers?: { [key: string]: string };

  constructor(
    name: string,
    politicalSituation?: PoliticalSituation,
    picture?: string,
    flag?: string,
    todoList?: Todo[],
    recentHistory?: string[],
    localLaws?: string[],
    itemsToBring?: string[],
    continent?: string,
    emergencyNumbers?: { [key: string]: string },
  ) {
    this.name = name;
    this.flag = flag || '';
    this.politicalSituation = politicalSituation;
    this.recentHistory = recentHistory || [];
    this.localLaws = localLaws || [];
    this.todoList = todoList || [];
    this.itemsToBring = itemsToBring || [];
    this.picture = picture || '';
    this.continent = continent || '';
    this.emergencyNumbers = emergencyNumbers || {};
  }

  private static ParseTodoArray(objs: any[]): Todo[] {
    return objs ? objs.map<Todo>(value => Todo.ParseFromObject(value)) : [];
  }

  static ParseFromObject(obj: any): Country {
    const country: Country = new Country(obj.name);

    country.politicalSituation = PoliticalSituation.ParseFromObject(obj.politicalSituation);
    country.picture = obj.picture;
    country.recentHistory = obj.recentHistory || [];
    country.localLaws = obj.localLaws || [];
    country.todoList = this.ParseTodoArray(obj.todoList);
    country.itemsToBring = obj.itemsToBring || [];
    country.description = obj.description;
    country.id = obj.id;
    country.flag = obj.flag;
    country.continent = obj.continent;
    country.emergencyNumbers = obj.usefulNumbers;
    return country;
  }
}
