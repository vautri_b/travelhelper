
export class UserDataModel {

  public countryItems: { [key: string]: string[] };

  constructor(countryItems?: { [key: string]: string[] }) {
    this.countryItems = countryItems || {};
  }
}
