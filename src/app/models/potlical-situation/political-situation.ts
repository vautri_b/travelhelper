export enum Stability {
  STABLE,
  UNSTABLE,
  SAFE
}

export class PoliticalSituation {

  public stability: Stability;
  public comment: string;

  constructor(stability: Stability, comment: string) {
    this.stability = stability;
    this.comment = comment;
  }

  static ParseFromObject(obj: any) {
    return new PoliticalSituation(obj.stability, obj.comment);
  }
}
