// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBSxffxgDhkCZb2c7p3Lo9YXjpICyFY8eQ",
    authDomain: "travelhelper2.firebaseapp.com",
    databaseURL: "https://travelhelper2.firebaseio.com",
    projectId: "travelhelper2",
    storageBucket: "travelhelper2.appspot.com",
    messagingSenderId: "758391370261",
    appId: "1:758391370261:web:560fd911b20cdf9e"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
